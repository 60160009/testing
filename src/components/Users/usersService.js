import axios from 'axios'

const url = 'http://localhost:3000/users/'

var usersService = {
  userList: [],
  addNewUser: function (user) {
    return axios.post(url, user).then(res => {
      return res.data
    })
  },
  updateUser: function (user) {
    return axios.put(url, user).then(res => {
      return res.data
    })
  },
  getUsers: async function () {
    await axios
      .get(url)
      .then(res => {
        this.userList = res.data
      })
      .catch(err => {
        console.log(err)
      })
    return this.userList
  },
  deleteUser: function (id) {
    return axios.delete(url + id).then(res => {
      return res.data
    })
  }
}
export default usersService
